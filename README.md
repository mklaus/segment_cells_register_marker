# Segment cells and register markers

This tool loads immuno-fluorescence images which are marked with
in-situ hybridisation signals. Theses signals were manually
annotated and are saved in .xml files.
The script uses now the images and the .xml files to first segment
the cells and then register and count the different signals in the cells.

## Cell segmentation
The cell nuclei are segmented using the blue channel which corresponds to 
the cell nuclei staining channel. First the skimage sobel filter is used to 
get an elevation map. On this elevation map a watershed segmentation is applied.
This is achieved by providing a lower pixel threshold (background class) and an upper pixel threshold (cell nuclei class)
as parameter to the watershed function.
Afterwards from the obtained segmentation an instance segmentation is computed.
This means that connected pixel components are perceived as one cell (a cell instance) and background
pixels separate the cells from each other.
The following post-processing operations are applied:
- Holes in segmented cells are filled.
- All cells which touch the edge of the images are removed.
- Furthermore cells which contain less or more pixels than biological meaningfull thresholds are removed.

## Marker registration
The x and y coordinates of the markers are used to register the markers in the segmented cells.
For each cell instance the number of markers which are in the cell is computed.
Finally an excel sheet is exported which contains the count of all registered markers in the segmented cells of all the images in the processed folder.

## Used packages
Python 3.7 and the following packages were used:

- [__numpy__](http://numpy.org): The fundamental package for scientific computing.

- [__scipy__](https://scipy.org): SciPy is a Python-based open-source software for mathematics, science, and engineering.

- [__scikit-image__](https://scikit-image.org/): scikit-image is a collection of algorithms for image processing.

- [__pandas__](https://pandas.pydata.org): The library which provides high-performance, easy-to-use data structures and data analysis tools for Python.

- [__matplotlib__](https://matplotlib.org): a Python 2D plotting library which produces figures

- [__openpyxl__](): Export dataframe as excel sheet
