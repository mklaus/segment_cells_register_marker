import pandas as pd
import numpy as np


def markers_in_instance_segmentation_dataframe(instance_array, markers):
    """
    instance_array: array (2dim)
     0 is the background
     1 to n are the foreground cell instances
    """
    # Array with all instances
    instances = np.unique(instance_array)

    # Dataframe dict
    dataframe_dict = {}

    # Create the cell_names column
    cell_names_key = 'cell_instance'
    dataframe_dict[cell_names_key] = []
    for instance in np.nditer(instances):
        if instance != 0:
            # Do not add 0 since it is the background
            cell_name = "cell " + str(instance)
            dataframe_dict[cell_names_key].append(cell_name)
            #print(instance)
    print(dataframe_dict[cell_names_key])

    # Create the signal columns
    for key in markers:
        signal_cols_key = "signal " + str(key)
        dataframe_dict[signal_cols_key] = np.zeros(len(dataframe_dict[cell_names_key]))
        print(signal_cols_key)
        print(dataframe_dict[signal_cols_key])
        for point in markers[key]:
            #print(point)
            for instance in np.nditer(instances):
                if instance != 0:
                    # Do not add 0 since it is the background
                    if instance_array[int(point[1]), int(point[0])] == instance:
                        dataframe_dict[signal_cols_key][instance - 1] += 1

    # Put it together to get the dataframe
    df = pd.DataFrame(dataframe_dict)

    return df


def merge_dataframes(project_dict):
    """
    project_dict[key]['df'] contains the dataframe for each key
    Merge all dataframes and return a df containing all dfs
    """

    frames = []
    keys = []
    # Iterate over all keys in project_dict
    for key in project_dict:
        frames.append(project_dict[key]['df'])
        keys.append(key)
    all_df = pd.concat(frames, keys=keys, sort=False)

    return all_df


if __name__ == "__main__":
    inst_array = a = np.array([[0, 0, 0, 1, 0],
                                   [2, 2, 2, 0, 0],
                                   [2, 2, 2, 0, 3]], dtype=int)
    markers_in_instance_segmentation_dataframe(inst_array)
