import os
import numpy as np
import skimage

from xml_to_dict import xml_to_dict
from segment_image import segment_image
from register_markers import print_markers_on_image
from get_dataframe import markers_in_instance_segmentation_dataframe, merge_dataframes
from visualization import plot_image


class Config:
    XML_FOLDER = '/home/martin/git/segment_cells_register_marker/data/xml/'
    IMG_FOLDER = '/home/martin/git/segment_cells_register_marker/data/images/'
    SAVE_FOLDER = '/home/martin/git/segment_cells_register_marker/results/'
    PLOT_SAVE_FOLDER = 'plots'

    IMG_FORMAT = ".jpg"

    # Segmentation settings
    # 0 for red, 1 for green, 2 for blue channel
    SEGM_CHANNEL = 2

    # Intensity of the segmentation channel in percentage
    WATERSHED_LOWER_MARKER_TH = 0.1
    WATERSHED_UPPER_MARKER_TH = 0.4

    FILL_HOLES = True
    REMOVE_SEGMENTATIONS_AT_EDGES = True

    # Threshold size in pixel
    MICRON_RESOLUTION = 6.4362  # in pixels per micron. Must be adjusted to the images!!!
    # wikipedia.de: For mammals a cell nucleus has 5 - 16 microns in diameter
    # That means the minimal area should be about
    # r=2.5, r^2 * pi ~= 18 microns2
    # That means the maximal area should be about
    # r=8, r^2 * pi ~= 200 microns2
    # In the images the bigger cells have a diameter of 8 microns
    # r=4, r^2 * pi ~= 50 microns2

    LOWER_OBJECT_SIZE_TH = 500  # ~12 microns2 here
    UPPER_OBJECT_SIZE_TH = 7500  # ~180 microns2 here

    # Registration settings
    POINT_RADIUS = 5

    SHOW_PLOTS = False
    SAVE_PLOTS = True # If SHOW_PLOTS is TRUE SAVE_PLOTS will be skipped
    SAVE_PLOT_FORMAT = ".png"
    SAVE_DF = True



cfg = Config

# Get list of all xml files in the xml folder
xml_files = os.listdir(cfg.XML_FOLDER)
xml_files.sort()
print(xml_files)
# Get list of all image files in the image folder
img_files = os.listdir(cfg.IMG_FOLDER)
img_files.sort()
print(img_files)

# Create a dict for each id with
# dict[sample_id] = {'markers': marker_dict, 'image': array, '
#                    'segmented_instance_image': array, foreground_image': array,
#                    'df': pandas dataframe}
project_dict = {}

# Iterate over the xml files
for file in xml_files:
    sample_id = file[:-4]
    # Create key in project_dict
    project_dict[sample_id] = {}

    # Load corresponding xml file
    project_dict[sample_id]['markers'] = xml_to_dict(os.path.join(cfg.XML_FOLDER, file))

    # Load corresponding image
    try:
        img_name = sample_id + cfg.IMG_FORMAT
        img_arr = skimage.io.imread(os.path.join(cfg.IMG_FOLDER, img_name))

        # Check datatype! Has to be uint8 and RGB
        if img_arr.dtype != 'uint8':
            raise TypeError("Datatype of image must be uint8. Please check the image")
        if img_arr.shape[2] != 3:
            raise TypeError("Image must have red, green and blue channel at the third axis [x,y,c]. "
                            "Please check the image")

        project_dict[sample_id]['image'] = img_arr
    except FileNotFoundError:
        print("No corresponding image to the xml file exists.",
              "xml-file:", file, "missing img-file:", img_name)

    # Get corresponding segmented instance image
    project_dict[sample_id]['instance_segmentation_image'] = segment_image(project_dict[sample_id]['image'], sample_id,
                                                                           cfg)

    # Remove the background from the image
    zero_background = (project_dict[sample_id]['instance_segmentation_image'] != 0)
    zero_background_three_channels = np.dstack((zero_background, zero_background, zero_background))
    project_dict[sample_id]['foreground_image'] = project_dict[sample_id]['image'] * zero_background_three_channels
    plot_image(project_dict[sample_id]['foreground_image'], cfg, title="foreground_image_" + sample_id)

    # Print markers on the image
    print_markers_on_image(project_dict[sample_id]['image'], project_dict[sample_id]['markers'], 'image_' + sample_id, cfg)

    # Print markers only on the segmented instances
    print_markers_on_image(project_dict[sample_id]['foreground_image'], project_dict[sample_id]['markers'], 'instances_' + sample_id,
                           cfg, instance_array=project_dict[sample_id]['instance_segmentation_image'])

    # Create dataframe containing info about cell instances and markers in there
    project_dict[sample_id]['df'] = markers_in_instance_segmentation_dataframe(
        project_dict[sample_id]['instance_segmentation_image'], project_dict[sample_id]['markers'])

    if cfg.SAVE_DF:
        project_dict[sample_id]['df'].to_excel(os.path.join(cfg.SAVE_FOLDER, sample_id + '.xlsx'))


# Merge all dataframes to one dataframe containing the info of all images in the folder
all_df = merge_dataframes(project_dict)
if cfg.SAVE_DF:
    all_df.to_excel(os.path.join(cfg.SAVE_FOLDER, 'all_df.xlsx'))
