from skimage.draw import circle

from visualization import plot_image


def print_markers_on_image(image, markers, title, cfg, instance_array=None, ignore_z=True):
    """
    print markers from the dict on the image



   instance_array: array (2dim)
        0 is the background
        1 to n are the foreground cell instances
        If instance_array is applied only the markers in the foreground instances are printed !

    ignore_z: bool (optional)
        ignore the z-coordinate in the markers dict.
        If True all channels are overprinted
    """
    #print(markers)
    for key in markers:
        image_with_markers = image.copy()
        #print(key)
        for point in markers[key]:
            #print(point)
            if ignore_z:
                # Attention! ImageJ uses other axis names. x and y need to be switched!
                if instance_array is not None:
                    if instance_array[int(point[1]), int(point[0])] > 0:
                        rr, cc = circle(int(point[1]), int(point[0]), cfg.POINT_RADIUS,
                                        shape=image_with_markers.shape[:2])
                        image_with_markers[rr, cc, :] = 255
                else:
                    rr, cc = circle(int(point[1]), int(point[0]), cfg.POINT_RADIUS, shape=image_with_markers.shape[:2])
                    image_with_markers[rr, cc, :] = 255
            else:
                raise ValueError("using z coordinate not implemented yet.")
        plot_image(image_with_markers, cfg, title=title + "_marker" + key)
    return

