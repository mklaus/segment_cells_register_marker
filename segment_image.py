import numpy as np
import skimage
from skimage.filters import sobel
from skimage.morphology import watershed, remove_small_objects
from skimage.color import label2rgb
from scipy import ndimage as ndi

from visualization import plot_segmentation_process


def _check_dtype_supported(ar):
    # Should use `issubdtype` for bool below, but there's a bug in numpy 1.7
    if not (ar.dtype == bool or np.issubdtype(ar.dtype, np.integer)):
        raise TypeError("Only bool or integer image types are supported. "
                        "Got %s." % ar.dtype)


def remove_big_objects(ar, max_size, connectivity=1, in_place=False):
    """ Adaption of https://github.com/scikit-image/scikit-image/blob/master/skimage/morphology/misc.py#L51
    Remove objects bigger than the specified size.
    Expects ar to be an array with labeled objects, and removes objects
    bigger than max_size. If `ar` is bool, the image is first labeled.
    This leads to potentially different behavior for bool and 0-and-1
    arrays.
    Parameters
    ----------
    ar : ndarray (arbitrary shape, int or bool type)
        The array containing the objects of interest. If the array type is
        int, the ints must be non-negative.
    max_size : int
        The biggest allowable object size.
    connectivity : int, {1, 2, ..., ar.ndim}, optional (default: 1)
        The connectivity defining the neighborhood of a pixel. Used during
        labelling if `ar` is bool.
    in_place : bool, optional (default: False)
        If ``True``, remove the objects in the input array itself.
        Otherwise, make a copy.
    Raises
    ------
    TypeError
        If the input array is of an invalid type, such as float or string.
    ValueError
        If the input array contains negative values.
    Returns
    -------
    out : ndarray, same shape and type as input `ar`
        The input array with small connected components removed.
    Examples
    --------
    >>> a = np.array([[0, 0, 0, 1, 0],
    ...               [1, 1, 1, 0, 0],
    ...               [1, 1, 1, 0, 1]], bool)
    >>> b = remove_big_objects(a, 5)
    >>> b
    array([[False, False, False, True, False],
           [ False,  False,  False, False, False],
           [ False,  False,  False, False, True]], dtype=bool)
    >>> c = remove_big_objects(a, 6, connectivity=2)
    >>> c
    array([[False, False, False,  False, False],
           [ False,  False,  False, False, False],
           [ False,  False,  False, False, True]], dtype=bool)
    >>> d = remove_big_objects(a, 6, in_place=True)
    >>> d is a
    True
    """
    # Raising type error if not int or bool
    _check_dtype_supported(ar)

    if in_place:
        out = ar
    else:
        out = ar.copy()

    if max_size == 0:  # shortcut for efficiency
        return out

    if out.dtype == bool:
        selem = ndi.generate_binary_structure(ar.ndim, connectivity)
        ccs = np.zeros_like(ar, dtype=np.int32)
        ndi.label(ar, selem, output=ccs)
    else:
        ccs = out

    try:
        component_sizes = np.bincount(ccs.ravel())
    except ValueError:
        raise ValueError("Negative value labels are not supported. Try "
                         "relabeling the input with `scipy.ndimage.label` or "
                         "`skimage.morphology.label`.")

    if len(component_sizes) == 2 and out.dtype != bool:
        print("WARNING. Only one label was provided to `remove_small_objects`. "
             "Did you mean to use a boolean array?")

    too_big = component_sizes > max_size
    too_big_mask = too_big[ccs]
    out[too_big_mask] = 0

    return out


def remove_segmentations_at_edges(ar, connectivity=1, in_place=False):
    """ Remove objects which touch the edges of the image.
    Expects ar to be an array with labeled objects, and removes objects
    that touch the edges. If `ar` is bool, the image is first labeled.
    This leads to potentially different behavior for bool and 0-and-1
    arrays.
    Parameters
    ----------
    ar : ndarray (2dim shape, int or bool type)
        The array containing the objects of interest. If the array type is
        int, the ints must be non-negative.
    connectivity : int, {1, 2, ..., ar.ndim}, optional (default: 1)
        The connectivity defining the neighborhood of a pixel. Used during
        labelling if `ar` is bool.
    in_place : bool, optional (default: False)
        If ``True``, remove the objects in the input array itself.
        Otherwise, make a copy.
    Raises
    ------
    TypeError
        If the input array is of an invalid type, such as float or string.
    ValueError
        If the input array contains negative values.
    Returns
    -------
    out : ndarray, same shape and type as input `ar`
        The input array with connected components which touch the edges removed.
    Examples
    --------
    >>> a = np.array([[0, 0, 0, 1, 0],
    ...               [0, 1, 1, 0, 0],
    ...               [0, 0, 0, 0, 1]], bool)
    >>> b = remove_segmentations_at_edges(a)
    >>> b
    array([[False, False, False, False, False],
           [ False,  True,  True, False, False],
           [ False,  False,  False, False, False]], dtype=bool)
    >>> c = remove_segmentations_at_edges(a, connectivity=2)
    >>> c
    array([[False, False, False,  False, False],
           [ False,  False,  False, False, False],
           [ False,  False,  False, False, False]], dtype=bool)
    >>> d = remove_segmentations_at_edges(a, in_place=True)
    >>> d is a
    True
    """
    # Raising type error if not int or bool
    _check_dtype_supported(ar)
    if len(ar.shape) != 2:
        raise TypeError("Array must be 2 dimensional here but has shape:", ar.shape)

    if in_place:
        out = ar
    else:
        out = ar.copy()

    if out.dtype == bool:
        selem = ndi.generate_binary_structure(ar.ndim, connectivity)
        ccs = np.zeros_like(ar, dtype=np.int32)
        ndi.label(ar, selem, output=ccs)
    else:
        ccs = out

    #print(ar)
    #print(ccs)

    # Edge array is 1 at the edges, else 0
    edge_arr = np.zeros_like(ar, np.int32)
    #print(edge_arr)
    edge_arr[0, :] = 1
    edge_arr[:, 0] = 1
    edge_arr[-1, :] = 1
    edge_arr[:, -1] = 1
    #print(edge_arr)

    # Find connnected components which overlap with edge array
    overlap_arr = ccs * edge_arr
    edge_components = np.unique(overlap_arr)
    #print(edge_components)
    # Find coordinates of these edge_components
    edge_components_coordinates = np.isin(ccs, edge_components)
    #print(edge_components_coordinates)
    # Set these edge_components to 0
    out[edge_components_coordinates] = 0
    #print(out)
    return out


def segment_image(img_arr, sample_id, config):
    #Channel to segment
    img_ch = img_arr[:, :, config.SEGM_CHANNEL]

    # Elevation map
    elevation_map = sobel(img_ch)

    # Find foreground and background markers
    markers = np.zeros_like(img_ch)
    markers[img_ch < (config.WATERSHED_LOWER_MARKER_TH*255)] = 1
    markers[img_ch > (config.WATERSHED_UPPER_MARKER_TH*255)] = 2

    # Do watershed (Let background be 0 and foreground be 1)
    segmented_image = watershed(elevation_map, markers) - 1

    # Process segmentations further
    processed_segmentation = segmented_image

    # Fill holes
    if config.FILL_HOLES:
        processed_segmentation = ndi.binary_fill_holes(processed_segmentation)

    # Remove segmentations which touch the edges
    if config.REMOVE_SEGMENTATIONS_AT_EDGES:
        processed_segmentation = remove_segmentations_at_edges(processed_segmentation)

    # Filter out small objects and big objects (works since filled_segmentation is binary array)
    processed_segmentation = remove_small_objects(processed_segmentation, config.LOWER_OBJECT_SIZE_TH)
    processed_segmentation = remove_big_objects(processed_segmentation, config.UPPER_OBJECT_SIZE_TH)

    # Get instances
    labeled_segmentation, number_instances = ndi.label(processed_segmentation)
    label_overlay_image = label2rgb(labeled_segmentation, image=img_arr)

    # Plot segmentation process
    plot_segmentation_process([img_arr, elevation_map, markers, segmented_image, processed_segmentation,
                               label_overlay_image], config, save_title=sample_id)

    return labeled_segmentation


if __name__ == "__main__":
    class Config:
        # 0 for red, 1 for green, 2 for blue channel
        SEGM_CHANNEL = 2

        # Intensity of the segmentation channel in percentage
        WATERSHED_LOWER_MARKER_TH = 0.2
        WATERSHED_UPPER_MARKER_TH = 0.55

        FILL_HOLES = True
        REMOVE_SEGMENTATIONS_AT_EDGES = True

        # Threshold size in pixel
        LOWER_OBJECT_SIZE_TH = 1000
        UPPER_OBJECT_SIZE_TH = 2000

    cfg = Config()

    path = '/home/martin/git/segment_cells_register_marker/data/images/HNE10_1-1.jpg'
    img_arr = skimage.io.imread(path)

    # Select the blue channel to segment
    labeled_segmentation = segment_image(img_arr, cfg)
