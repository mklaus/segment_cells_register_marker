import matplotlib.pyplot as plt
import os


def plot_image(img_arr, cfg, title=None, cmap=plt.cm.gray):
    plt.figure(1, figsize=(6, 6))
    if title is not None:
        plt.title(title)
    plt.imshow(img_arr, cmap=cmap)
    if cfg.SHOW_PLOTS:
        plt.show()
    elif cfg.SAVE_PLOTS:
        plt.savefig(os.path.join(cfg.SAVE_FOLDER, cfg.PLOT_SAVE_FOLDER, title + cfg.SAVE_PLOT_FORMAT))
        plt.close()
    return


def plot_segmentation_process(img_arr_list, cfg, save_title=None, cmap=plt.cm.gray):
    fig, axes = plt.subplots(3, 2, figsize=(6, 9), sharex=True, sharey=True)
    axes[0, 0].imshow(img_arr_list[0], cmap=cmap)
    axes[0, 0].set_title('original image')
    axes[0, 1].imshow(img_arr_list[1], cmap=cmap)
    axes[0, 1].set_title('elevation map')
    axes[1, 0].imshow(img_arr_list[2], cmap=cmap)
    axes[1, 0].set_title('watershed markers')
    axes[1, 1].imshow(img_arr_list[3], cmap=cmap)
    axes[1, 1].set_title('segmented image')
    axes[2, 0].imshow(img_arr_list[4], cmap=cmap)
    axes[2, 0].set_title('processed segmented image')
    axes[2, 1].imshow(img_arr_list[5], cmap=cmap)
    axes[2, 1].set_title('labeled overlay image')
    if cfg.SHOW_PLOTS:
        plt.show()
    elif cfg.SAVE_PLOTS:
        plt.savefig(os.path.join(cfg.SAVE_FOLDER, cfg.PLOT_SAVE_FOLDER, "segementation_process_" + save_title + cfg.SAVE_PLOT_FORMAT))
        plt.close()
    return

