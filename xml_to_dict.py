import xmltodict


def xml_to_dict(path):
    """
    Import xml file of an image processed with IJ CellCounter
    and get the interesting information out of it.
    """

    with open(path) as fd:
        doc = xmltodict.parse(fd.read())

    print("Image name:", doc['CellCounter_Marker_File']['Image_Properties']['Image_Filename'])

    # Create dict that will be returned
    cell_counter_dict = {}

    # Get list containing all the marker info
    marker_data = doc['CellCounter_Marker_File']['Marker_Data']['Marker_Type']

    for marker in marker_data:
        # Print name of marker
        print("Name of marker: ", marker['Type'])
        # Print element list of marker
        try:
            print("Element list of marker", marker['Type'], ":", marker['Marker'])

            # Convert OrderedDict to n-tupel with the format (x,y,z)
            list_of_ntupels = []
            # marker['Marker'] is usually a list containing OrderedDict
            if type(marker['Marker']) is list:
                for point in marker['Marker']:
                    x = point['MarkerX']
                    y = point['MarkerY']
                    z = point['MarkerZ']
                    list_of_ntupels.append((x, y, z))
            # If it contains only one element it is not a list but already a "point"
            else:
                print("marker['Marker'] is no list here!")
                print(marker['Marker'])
                x = marker['Marker']['MarkerX']
                y = marker['Marker']['MarkerY']
                z = marker['Marker']['MarkerZ']
                list_of_ntupels.append((x, y, z))
                print("list_of_ntupels", list_of_ntupels)

            # Add list for each marker in the dict
            cell_counter_dict[marker['Type']] = list_of_ntupels
        except KeyError:
            print("Marker", marker['Type'], "has no elements")

    return cell_counter_dict


if __name__ == '__main__':
    print(xml_to_dict('/home/martin/git/segment_cells_register_marker/data/xml/HNE10_1-1.xml'))
